function toggleMRsWithoutMilestone() {
  document.querySelectorAll('table tr[data-milestone="None"]').forEach(function(x) {
    if(x.classList.contains('hide-empty-milestone')) {
      x.classList.remove('hide-empty-milestone');
    }
    else {
      x.classList.add('hide-empty-milestone');
    }
  })
};
