# gitlab-org/distribution/monitoring

This is a collection of scripts used to monitor and alert on various Distribution team tasks.

## Which MRs are listed

Merge requests are included on the monitoring dashboard if all of the following conditions are met:

- comes from a project in the `gitlab-org` group
- comes from a [Distribution-owned project](lib/data_sources/projects.yaml)
- is currently in the `open` state
- is labeled `~workflow::ready for review`

## Scripts

### scripts/merge_requests/ready_for_review

Checks the list of merge requests currently waiting for review by the Distribution team, and alerts on any review that is in breach of the []First Response SLO](https://about.gitlab.com/handbook/engineering/workflow/code-review/#first-response-slo)


## Development

This project is composed of a static site that displays data and scripts that are run periodically by GitLab CI to update the data that the static site consumes and displays.

We are using [Nanoc](https://nanoc.app/about/) as static site generator.

Run the website locally with these commands:

1. Create a personal GitLab access token with `read_api` scope and set the environment variable `PRIVATE_TOKEN` to the value of your access token.
1. `bundle install`, install gem dependencies.
1. `bundle exec nanoc compile`, compile static site assets into `output/`.
1. `bundle exec nanoc view -L`, start static webserver on default port 3000
1. Open http://localhost:3000 to view the site.

`bundle exec nanoc compile` must be run after file changes to generate new output assets.
Compiling in one terminal and running `bundle exec nanoc view -L` in another terminal enables live-reloading changes on compile.
