require 'business_time'
require 'date'
require 'httparty'
require 'yaml'

def project_ids
  return YAML.load_file("lib/data_sources/projects.yaml")
end

def project_pipelines
  @items.find_all('/pipelines/project/*')
end

def pipelines_by_status(pipelines, status)
  pipelines.select { |p| p[:status] == status }
end

def pipelines_durations(pipelines)
  pipelines_with_duration = pipelines.reject{|p| p[:duration].nil? || p[:duration] < 60}
  return [] if pipelines_with_duration.length == 0

  pipelines_with_duration.map{|p| p[:duration] / 60}.sort
end

def percent(numerator, denominator)
  (numerator.to_f / denominator * 100).to_i
end

def median(array)
  return nil if array.empty?
  sorted = array.sort
  len = sorted.length
  ((sorted[(len - 1) / 2] + sorted[len / 2]) / 2.0).to_i
end

def ready_for_review
  @items.find_all('/mrs/ready_for_review/*').sort_by { |x| x[:ready_at] }
end

def pipeline_failures
  @items.find_all('/pipeline_failures/failures/*').sort_by { |x| x[:created_at] }
end

def color(labeled)
  # Based on the Distribution Team SLO:
  #   https://about.gitlab.com/handbook/engineering/development/enablement/systems/distribution/merge_requests.html#service-level-objective
  return 'red' if DateTime.now > 4.business_days.after(labeled)
  return 'yellow' if DateTime.now > 2.business_days.after(labeled)

  'green'
end

def alert_enabled?
  required_values = %w(ALERT_URL ALERT_KEY)
  (ENV.keys & required_values).length == required_values.length
end

def mr_report_url
  "#{ENV['CI_PAGES_URL']}/mrs"
end

def alert(merge_request)
  # Do nothing if we don't have a url for alerts
  return unless alert_enabled?

  content = {
    headers: {
      'Content-Type': 'application/json',
      'Authorization': "Bearer #{ENV['ALERT_KEY']}"
    },
    body: {
      title: 'Merge Request Needs Response',
      description: "#{merge_request.web_url} has breached our First response SLO, please check.",
      fingerprint: merge_request.iid
    }.to_json
  }
  results = HTTParty.post(URI(ENV['ALERT_URL']), content)
  raise "Error posting alert: #{results.code} - #{results.message}" unless results.code == 200
end

def report_run_date
  DateTime.parse(Time.now.utc.to_s)
end
