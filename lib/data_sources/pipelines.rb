require 'date'
require 'httparty'
require 'json'

class Pipelines < ::Nanoc::DataSource
  identifier :pipelines

  MONTHS_OF_DATA = 1

  def token
    raise 'No token provided, please set PRIVATE_TOKEN if calling manually' unless ENV.key?('PRIVATE_TOKEN')

    ENV['PRIVATE_TOKEN']
  end

  def endpoint
    ENV.key?('GITLAB_ENDPOINT') ? ENV['GITLAB_ENDPOINT'] : 'https://gitlab.com/api/graphql'
  end

  def up
    @list = []
    updatedAfter = DateTime.now.prev_month(MONTHS_OF_DATA).strftime('%Y-%m-%d')

    project_ids.each do |project_id|
      project_item = {iid: project_id, pipelines: []}
      has_next_page = true
      cursor = nil

      while has_next_page
        query = ERB.new(
          File.read('lib/data_sources/pipelines.graphql.erb')
        ).result_with_hash(projects: [project_id], updatedAfter: updatedAfter, cursor: cursor)

        raw_response = HTTParty.post(endpoint, {
          format: :plain,
          headers: {
            "Authorization": "Bearer #{token}",
            "Content-Type": "application/json"
          },
          body: { "query": query }.to_json
        })

        resp = JSON.parse raw_response, symbolize_names: true

        project_resp = resp[:data][:projects][:nodes].first
        main_branch = project_resp[:master][:nodes].length > 0 ? :master : :main
        pipelines = project_resp[main_branch][:nodes]
        pageInfo = project_resp[main_branch][:pageInfo]

        project_item[:name] = project_resp[:name]
        project_item[:fullPath] = project_resp[:fullPath]
        project_item[:webUrl] = project_resp[:webUrl]
        project_item[:branch] = main_branch
        project_item[:pipelines].push(*pipelines)

        has_next_page = pageInfo[:hasNextPage]
        cursor = pageInfo[:endCursor]
      end

      if project_item[:pipelines].length > 0
        project_item[:pipelines_count_total] = project_item[:pipelines].length
        project_item[:pipelines_count_failed] = pipelines_by_status(project_item[:pipelines], 'FAILED').length
        project_item[:pipelines_count_success] = pipelines_by_status(project_item[:pipelines], 'SUCCESS').length
        project_item[:pipelines_percent_success] = percent(project_item[:pipelines_count_success], project_item[:pipelines_count_total])

        durations = pipelines_durations(pipelines_by_status(project_item[:pipelines], 'SUCCESS'))
        project_item[:pipelines_duration_min] = durations.first
        project_item[:pipelines_duration_median] = median(durations)
        project_item[:pipelines_duration_max] = durations.last

        @list.append(project_item)
      end
    end
  end

  def items
    @list.map do |project|
      new_item(
        '',
        project,
        "/project/#{project[:iid]}"
      )
    end
  end
end
