require 'gitlab'

class PipelineFailures < ::Nanoc::DataSource
  identifier :pipeline_failures

  def time_elapsed(timestamp)
    now = DateTime.now

    elapsed_seconds = now.to_time - timestamp.to_time

    hour_seconds = 60 * 60
    day_seconds = hour_seconds * 24

    {
      'hours' => (elapsed_seconds / hour_seconds),
      'days' => (elapsed_seconds / day_seconds)
    }
  end

  def investigation_status(labels = [])
    return "Unknown" if labels.nil?

    status = labels.find do |label|
      label.include? 'pipeline failure::'
    end

    return "Unknown" if status.nil?

    status.sub('pipeline failure::', '')
  end

  def token
    raise 'No token provided, please set PRIVATE_TOKEN if calling manually' unless ENV.key?('PRIVATE_TOKEN')

    ENV['PRIVATE_TOKEN']
  end

  def endpoint
    ENV.key?('GITLAB_ENDPOINT') ? ENV['GITLAB_ENDPOINT'] : 'https://gitlab.com/api/v4'
  end

  def up
    @gl = Gitlab.client(
      endpoint: endpoint,
      private_token: token
    )

    failed_pipeline_labels = ['pipeline failure', 'group::distribution']

    @list = @gl.group_issues(
      'gitlab-org',
      {
        state: 'opened',
        labels: failed_pipeline_labels
      }
    ).auto_paginate.map do |issue|
      created_at = DateTime.parse(issue.created_at)
      elapsed = time_elapsed(created_at)
      state = color(created_at)
      status = investigation_status(issue.labels)
      {
        web_url: issue.web_url,
        title: issue.title,
        created_at: issue.created_at,
        age_hours: elapsed['hours'].round(0),
        age_days: elapsed['days'].round(1),
        project_id: issue.project_id,
        iid: issue.iid,
        reference: issue.references.full.gsub('gitlab-org/', ''),
        state: state,
        status: status,
        assignee: issue.assignee
      }
    end

    @list.delete_if { |issue| !project_ids.include?(issue[:project_id]) }
    @list.delete_if { |issue| !issue[:assignee].nil? }
  end

  def items
    @list.map do |issue|
      new_item(
        '',
        issue,
        "/failures/#{issue[:iid]}"
      )
    end
  end
end
