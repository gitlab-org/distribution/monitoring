require 'gitlab'

class MergeRequests < ::Nanoc::DataSource
  identifier :mrs

  def labeled_at(merge_request, label)
    raise "MR #{merge_request.iid} does not have #{label} label" unless merge_request.labels.include?(label)

    last_time = DateTime.parse(merge_request.created_at)

    @gl.merge_request_label_events(merge_request.project_id, merge_request.iid)
       .auto_paginate do |mrle|
      next if mrle.label.nil? || !mrle.label.name.eql?(label)

      time =  DateTime.parse(mrle.created_at)
      last_time = time if time > last_time
    end

    last_time
  end

  def milestone(merge_request)
    return merge_request.milestone['title'] if merge_request.milestone

    "None"
  end

  def token
    raise 'No token provided, please set PRIVATE_TOKEN if calling manually' unless ENV.key?('PRIVATE_TOKEN')

    ENV['PRIVATE_TOKEN']
  end

  def endpoint
    ENV.key?('GITLAB_ENDPOINT') ? ENV['GITLAB_ENDPOINT'] : 'https://gitlab.com/api/v4'
  end

  def up
    @gl = Gitlab.client(
      endpoint: endpoint,
      private_token: token
    )

    label_ready_for_review = 'workflow::ready for review'

    @list = @gl.group_merge_requests(
      'gitlab-org',
      state: 'opened',
      labels: [label_ready_for_review]
    ).auto_paginate.map do |mr|
      ready_at = labeled_at(mr, label_ready_for_review)
      state = color(ready_at)
      alert(mr) if state.eql?('red')
      {
        web_url: mr.web_url,
        iid: mr.iid,
        reference: mr.references.full.gsub('gitlab-org/', ''),
        title: mr.title,
        author_name: mr.author.name,
        ready_at: ready_at,
        state: state,
        project_id: mr.project_id,
        milestone: milestone(mr)
      }
    end

    @list.delete_if { |mr| !project_ids.include?(mr[:project_id]) }
  end

  def items
    @list.map do |mr|
      new_item(
        '',
        mr,
        "/ready_for_review/#{mr[:project_id]}_#{mr[:iid]}"
      )
    end
  end
end
